import styles from 'styles/Home.module.scss'

const Home = () => {
  return (
    <div className={styles.mainContainer}>
      <div className={styles.box}>
        <div className={styles.row}>
          <div className={styles.leftContainer}>
            <div className={styles.column}>
              <span className={styles.title}>Tier 1: Rookie Fan</span>
              <span className={styles.subTitle}>Get 15 Tokens or more and get<br/>all the following perks</span>
            </div>
            <span className={styles.quantity}>15 MJS <span>₹600</span></span>
          </div>
          <div className={styles.rightContainer}>
            <img src='/user.png' alt='user_image' />
          </div>
        </div>
        <div className={styles.card} data-is-first={true}>
          <span>Be a part of my exclusive community on Listed</span>
        </div>
        <div className={styles.card}>
          <span>Get a shoutout in post-credit of an episode</span>
        </div>
        <div className={styles.card}>
          <span>Be a part of my exclusive community on Listed</span>
        </div>
      </div>
    </div>
  )
}

export default Home;